 from setuptools import setup
 import os

 # Put here required packages
 packages = []

 setup(name='graph_theory',
       version='0.1',
       description='graph_theory', # <= Put your description if you want
       author='Andrew Murray',          # <= Your name!!!!
       author_email='andyated@gmail.com',
       url='https://gitlab.com/andrew-murray/graph_theory',
       install_requires=packages,
 )
