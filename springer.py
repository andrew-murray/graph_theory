# -*- coding: utf-8 -*-
import requests
from requests.compat import urljoin, urlsplit
import grequests
from bs4 import BeautifulSoup
from parse import parse
import ref

def remap_article_url(url):
    parse_format = "http://link.springer.com/{article_type}/{0}/{1}"
    bibtex_format = "http://citation-needed.services.springer.com/v2/references/{0}/{1}?format=refman&flavour=citation"
    return bibtex_format.format(*parse(parse_format,url).fixed)

def parse_issues(response):
    bs = BeautifulSoup(response.text, "lxml")
    issues = bs.find_all("li", class_ = "issue-item" )
    issue_list = []
    for issue in issues:
        link = issue.a
        issue_url = urljoin( response.url, link["href"] )
        issue_text = link.text
        issue_list.append(issue_url)
    return issue_list

def parse_articles(response):
    bs = BeautifulSoup(response.text, "lxml")
    articles = (
        list(bs.find_all("div", class_ = "toc-item" ) ) +
        [ x for x in bs.find_all("li", class_ = "toc-item" ) if not "front-matter-item" in x['class'] and not "back-matter-item" in x["class"] ]
    )
    article_list = []
    for art in articles:
        article_header = art.find("a")
        article_title = article_header.text
        article_url = urljoin( response.url, article_header["href"] )
        article_list.append( article_url )
    # handle pagination by recursion
    if len(article_list) == 20:
        next_elements = bs.find_all("a", class_= "next") + bs.find_all("span", class_ = "next")
        if len(next_elements) >= 1 and next_elements[0].has_attr('href'):
            next_url = urljoin( response.url, next_elements[0]['href'] )
            article_list.extend( parse_articles(requests.get(next_url)))
    return article_list

def exception_handler(request, e):
    print "Request failed"
    print str(e)

def parse_journal_root(url, session = None):
    issue_page = session.get(url) if session else requests.get(url)
    issue_list = parse_issues(issue_page)
    return issue_list

def parse_issue_list(issue_list, session, async_limit = 5):
    article_list = []
    hooks = {'response': lambda resp, *args, **kwargs: article_list.extend(parse_articles(resp))}
    tasks = [grequests.get(issue, hooks = hooks, session = session) for issue in issue_list]
    grequests.map( tasks, size = async_limit, exception_handler=exception_handler )
    parsed_articles = []
    hooks = {'response': lambda resp, *args, **kwargs: parsed_articles.append(ref.parse_ris(resp.text))}
    bibtex_urls = [remap_article_url(a) for a in article_list]
    tasks = [grequests.get(bib, hooks = hooks, session = session) for bib in bibtex_urls]
    grequests.map( tasks, size = async_limit, exception_handler=exception_handler )
    return parsed_articles

def parse_journal(
    url,
    limit = None,
    session = None,
    async_limit = 5):
    real_session = requests.Session() if session is None else session
    issue_list = parse_journal_root(url, real_session)
    if limit:
        issue_list = issue_list[:limit]
    return parse_issue_list(issue_list, real_session, async_limit)
