# -*- coding: utf-8 -*-
import requests
from requests.compat import urljoin, urlsplit
import grequests
from bs4 import BeautifulSoup
from itertools import izip_longest
from parse import parse
import ref
import json
import selenium
try:
    import dryscrape
except Exception as e:
    class no_attrs:
        def __getattr__(self, attr):
            raise e
    dryscrape = no_attrs()

ARTICLES_TO_AVOID = [
    "Cover Image",
    "Masthead",
    "Issue Information"
]

def contains_black_list_text(node):
    for evil_word in ARTICLES_TO_AVOID:
        if evil_word in node.text:
            return True
    return False

def parse_citation_page(response):
    bs = BeautifulSoup(response.text, "xml", from_encoding=response.encoding)
    container = bs.find("pre")
    reference = container.text.encode("utf-8")
    return ref.parse_ris(reference)

def remap_article_url(url):
    # http://onlinelibrary.wiley.com/doi/10.1002/jmri.25341/full
    # http://onlinelibrary.wiley.com/enhanced/exportCitation/doi/10.1002/jmri.25341
    parse_format = "http://onlinelibrary.wiley.com/doi/{0}/full"
    final_url_format = "http://onlinelibrary.wiley.com/enhanced/exportCitation/doi/{0}"
    final_url = final_url_format.format(parse(parse_format, url.encode("utf-8")).fixed[0])
    return final_url

def parse_journal_content(response):
    bs = BeautifulSoup(response.text, from_encoding=response.encoding)
    toc_articles = bs.find_all("div", class_="tocArticle")
    article_links = [remap_article_url(urljoin(response.url,art.a['href'])) for art in toc_articles if not(contains_black_list_text(art))]
    return article_links

def parse_volume_page(response, root_url, from_dryscrape = False):
    bs = BeautifulSoup(response.text if from_dryscrape else response)
    issues = bs.find_all("div", class_="issue")
    # there are some zombie elements that are class_=issue but
    # are something else in the web page ... don't see a better way to distinguish
    issue_links = [ urljoin(root_url, issue.a['href'] ) for issue in issues if issue.a is not None ]
    return issue_links

def generate_issue_links(url, session=None):
    if session == None:
        session = dryscrape.Session()
        # note we need to load images, or the below clicks fail
        # session.set_attribute('auto_load_images', False)
    done = False
    for i in range(10):
        try:
            if not done:
                session.visit(url)
                for ln in session.xpath('//a[@class="issuesInYear closed"]'):
                    ln.click()
                done = True
        except Exception as e:
            last_e = e
    if not done:
        raise last_e
    response = session.body()
    issue_links = parse_volume_page(response, url)
    return issue_links

from selenium import webdriver

def generate_issue_links_selenium(url, session = None):
    ph = r"E:\externals\phantomjs-2.1.1-windows\phantomjs-2.1.1-windows\bin\phantomjs.exe"
    # browser = webdriver.Chrome(r"E:\externals\chromedriver.exe" )
    browser = webdriver.PhantomJS(ph)
    browser.get( url )
    # eh .... initialise the loop
    while True:
        elements = browser.find_elements_by_xpath('//a[@class="issuesInYear closed"]')
        if not elements:
            break
        try:
            elements[0].click()
        except selenium.common.exceptions.WebDriverException as e:
            # usually the click fails, because the page was updating, when we grabbed the old element
            # and then tried to click it ... and it had moved
            # there isn't a great way to manage this, so we just keep going
            pass
    response = browser.page_source
    issue_links = parse_volume_page(response, url)
    return issue_links


def parse_journal(url, session = None, batch_size = 5, limit = None):
    # this test only sometimes passes
    # in particular sometimes it seems to miss old issues
    issue_links = generate_issue_links(url,session = session)
    if limit:
        issue_links = issue_links[:limit]
    citation_pages = []
    push_article = lambda resp, *args, **kwargs: citation_pages.extend(parse_journal_content(resp))
    grequests.map( [ grequests.get(il, hooks = { 'response' : push_article }) for il in issue_links ], size = batch_size )
    citations = []
    push_citation = lambda resp, *args, **kwargs: citations.append(parse_citation_page(resp))
    grequests.map( [grequests.get(cp, hooks = {'response' : push_citation}) for cp in citation_pages], size = batch_size )
    return citations
