

def is_author_tag(x):
    return x == "AU" or ( len(x) == 2 
        and x[0] == "A" and ( x[1] == '1' or x[1] == '2' or x[1] == '3' or x[1] == '4' ) 
    )

def take_one(kvs, tag):
    return next( (kv[-1] for kv in kvs if kv[0] == tag) , None)

def parse_ris(text):
    keys_and_values = [[ t.strip() for t in x.partition("-")] for x in text.split("\n") if " - " in x]
    ref_type = take_one(keys_and_values, "TY")
    authors = [kv[-1] for kv in keys_and_values if is_author_tag(kv[0])]
    abstract = take_one(keys_and_values, "AB")
    date = take_one(keys_and_values, "DA")
    title = take_one(keys_and_values, "TI")
    journal = take_one(keys_and_values, "JO")
    volume = take_one(keys_and_values, "VL")
    issue = take_one(keys_and_values, "IS")
    start_page = take_one(keys_and_values, "SP")
    end_page = take_one(keys_and_values, "EP")
    url = take_one(keys_and_values, "UR")
    doi = take_one(keys_and_values, "DO")
    keywords = [kv[-1] for kv in keys_and_values if kv[0] == "KW"]
    serial = take_one(keys_and_values, "SN")
    pub_year = take_one(keys_and_values, "PY")
    journal_abbrev = take_one(keys_and_values, "JA")
    publisher = take_one(keys_and_values, "PB")
    editors = [kv[-1] for kv in keys_and_values if kv[0] == "ED"]
    book_title = take_one(keys_and_values, "BT")
    place_published = take_one(keys_and_values, "CY")
    ref_id = take_one(keys_and_values, "ID")
    # TODO reorder these so as to follow the order
    #      in the wikipedia , (which I'm currently using as a spec)
    return {
        "type" : ref_type,
        "journal" : journal,
        "journal_abbreviation" : journal_abbrev,
        "abstract" : abstract,
        "authors" : authors,
        "editors" : editors,
        "date" : date,
        "publication_year" : pub_year,
        "title" : title,
        "volume" : volume,
        "issue" : issue,
        "start_page" : start_page,
        "end_page" : end_page,
        "url" : url,
        "doi" : doi,
        "serial_number" : serial,
        "keywords" : keywords,
        "publisher" : publisher,
        "place_published" : place_published,
        "book_title" : book_title,
        "reference_id" : ref_id
    }