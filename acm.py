import requests
from bs4 import BeautifulSoup
import dryscrape
from requests.compat import urljoin, urlsplit
import bibtexparser
import itertools


SAMPLE_JOURNAL_ROOT = "http://dl.acm.org/citation.cfm?id=J778&preflayout=flat"
SAMPLE_ISSUE_PAGE = u'http://dl.acm.org/citation.cfm?id=3068851&preflayout=flat'


CITATION_PAGE = "http://dl.acm.org/exportformats.cfm?id=2546276&expformat=bibtex"

# CITATION_PAGE = "http://dl.acm.org/downformats.cfm?id=2546276&expformat=bibtex"

def fetch_issue_links(url, session = None):
    if session == None:
        session = dryscrape.Session()
        # note we need to load images, or the below clicks fail
        # session.set_attribute('auto_load_images', False)

    session.visit(url)
    tds = [
        x for x in session.xpath("//div[contains(@id,'fback')]//tr//td")
        if "Volume" in x.text() and "Issue" in x.text() and "Current Issue" not in x.text()
    ]

    links = [ urljoin( url, td.at_xpath("a").get_attr("href") ) for td in tds ]

    return links

import ipdb
def generate_article_pages_from_issues(url, session = None):
    if session == None:
        session = dryscrape.Session()
        # note we need to load images, or the below clicks fail
        # session.set_attribute('auto_load_images', False)
    session.visit(url)

    root_text = session.at_xpath("//table[contains(@class,'text12')]")
    ipdb.set_trace()
    link_rows = [ a.get_attr("href") for a in itertools.chain( ( x.at_xpath(".//a") for x in root_text.xpath(".//td") if x is not None )) if a is not None ]
    good_links = [ urljoin(url, l) for l in link_rows if "citation.cfm" in l ]
    return good_links


def parse_citation_page(url, session = None):
    if session == None:
        session = dryscrape.Session()

    session.visit(url)
    bibtex_text = session.at_xpath("//pre").text()
    return bibtexparser.loads( bibtex_text ).entries_dict

def reformat_article_link_as_bibtex(url):
    return url

def parse_journal(url, session = None, max_issues = None):
    if session == None:
        session = dryscrape.Session()
        # note we need to load images, or the below clicks fail
        # session.set_attribute('auto_load_images', False)
    issue_links = fetch_issue_links( url, session )
    if max_issues:
        issue_links = issue_links[:max_issues]

    return issue_links
    article_links = []
    for issue_ln in issue_links:
        article_links.extend( generate_article_pages_from_issues( issue_ln, session ) )
    return article_links
