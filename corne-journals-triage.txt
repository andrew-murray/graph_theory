
* SIAM Journal of Imaging Science - [http://epubs.siam.org/journal/sjisbi SIAM]
* Journal of Machine Learning Research - [http://www.jmlr.org Microtome]
* Neural Computation - [http://www.mitpressjournals.org/loi/neco MIT Press]
* Journal of Medical Imaging - [http://medicalimaging.spiedigitallibrary.org/journal.aspx?journalid=165 SPIE]


Clinical Focused

* Medical Physics - [http://scitation.aip.org/content/aapm/journal/medphys AIP]
* Physics in Medicine and Biology - [http://www.iop.org/Journals/0031-9155 IOP]
* Methods of Information in Medicine - [http://methods.schattauer.de/home.html Schattauer]
* Radiology - [http://pubs.rsna.org/journal/radiology RSNA]


Elsevier Technical

* Computer Vision and Image Understanding - [http://www.sciencedirect.com/science/journal/10773142 Elsevier]
* Pattern Recognition Letters - [http://www.sciencedirect.com/science/journal/01678655 Elsevier]
* Image & Vision Computing - [http://www.sciencedirect.com/science/journal/02628856 Elsevier]
* Medical Image Analysis - [http://www.sciencedirect.com/science/journal/13618415 Elsevier]
* Computerized Medical Imaging and Graphics - [http://www.sciencedirect.com/science/journal/08956111 Elsevier]
* NeuroImage - [http://www.sciencedirect.com/science/journal/10538119 Elsevier]
* Pattern Recognition - [http://www.sciencedirect.com/science/journal/00313203 Elsevier]
* Neural Networks - [http://www.sciencedirect.com/science/journal/08936080 Elsevier]

Elsevier CLinical


* NeuroImage: Clinical - [http://www.sciencedirect.com/science/journal/22131582 Elsevier]
* Computer Methods and Programs in Biomedicine - [http://www.sciencedirect.com/science/journal/01692607 Elsevier]
* Computers in Biology and Medicine - [http://www.sciencedirect.com/science/journal/00104825 Elsevier]
