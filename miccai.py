import requests
import grequests
from bs4 import BeautifulSoup
import springer
import json

def parse_past_proceedings():
	past_proceedings_url = "http://www.miccai.org/PastProceedings"
	pp_req = requests.get(past_proceedings_url)
	bs = BeautifulSoup(pp_req.text, "lxml")
	# most of the past proceedings are springer things
	# .. the rest I'll have to deal with another time
	all_links = [ x['href'] for x in bs.find_all("a") if x.has_attr("href") ]
	springer_links = [ l for l in all_links if "springer" in l and "link" in l]
	valid_springer_links = [ l for l in springer_links if "metapress" not in l]
	return valid_springer_links

def miccai_2015_proceedings():
	# src_link = https://miccai2015.org/frontend/index.php?folder_id=286
	# this time I copied the three links rather than 
	# writing a parser... seemed more efficient
	proceedings = [
		"http://link.springer.com/book/10.1007%2F978-3-319-24553-9",
		"http://link.springer.com/book/10.1007%2F978-3-319-24571-3",
		"http://link.springer.com/book/10.1007%2F978-3-319-24574-4"
	]
	return proceedings

def gather_springer_proceedings():
	return parse_past_proceedings() + miccai_2015_proceedings()

class MiccaiMonitor(object):
	def populate(self):
		proceedings = gather_springer_proceedings()
		articles = springer.parse_issue_list(proceedings)
		open("dump.json",'w').write(json.dumps(articles, indent = 4))