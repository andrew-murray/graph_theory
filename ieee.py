import requests
import grequests
from bs4 import BeautifulSoup


"""
	Ieee xplore api is xml and documented
	http://ieeexplore.ieee.org/gateway/
	http://www.programmableweb.com/api/ieee
"""
def parse_list(l):
	if l is None:
		return None
	else:
		return [ a.text.encode("utf-8") for a in l.find_all("term") ]

def find_or(doc, element, default = None):
	return doc.find(element) if doc.find(element) is not None else default

def find_str_or(doc, element, default = None):
	return doc.find(element).text.encode("utf-8") if doc.find(element) is not None else default

def parse_document(doc):
	return {
		"title" : find_str_or( doc, "title" ),
		"authors" : find_str_or( doc, "authors", "" ),
		"affiliations" : find_str_or( doc, "affiliations" ),
		"controlledterms" : parse_list(find_or( doc, "controlledterms")),
		"thesaurusterms" :  parse_list(find_or( doc, "thesaurusterms")),
		"pubtitle" : find_str_or( doc, "pubtitle" ),
		"punumber" : find_str_or( doc, "punumber"),
		"pubtype" : find_str_or( doc, "pubtype"),
		"publisher" : find_str_or(doc, "publisher" ),
		"volume" : find_str_or(doc, "volume" ),
		"publication_year" : find_str_or(doc, "py" ),
		"start_page" : find_str_or(doc, "spage" ),
		"end_page" : find_str_or(doc, "epage" ),
		"abstract" : find_str_or(doc, "abstract" ),
		"issn" : find_str_or(doc, "issn" ),
		"htmlFlag" : find_str_or(doc, "htmlFlag" ),
		"article_number" : find_str_or(doc, "arnumber" ),
		"doi" : find_str_or(doc, "doi" ),
		"publicatonId" : find_str_or(doc, "publicationId" ),
		"part_number" : find_str_or(doc, "partnum" ),
		"mdurl" : find_str_or(doc, "mdurl" ),
		"pdf" : find_str_or(doc, "pdf" )
	}

def fetch_all_articles(pub_number, batch_size=1000, current_index = 0, limit = None):
	# let's do this recursively, recursive feels more appropriate than a loop
	# # off by one maybe?
	required_batch_size = min( batch_size, limit * 20 - current_index )
	if required_batch_size == 0:
		return []

	# http://ieeexplore.ieee.org/gateway/ipsSearch.jsp?pn=2945&hc=100&rs=101
	url_format = "http://ieeexplore.ieee.org/gateway/ipsSearch.jsp?pn={pn}&hc={hc}&rs={rs}"
	resolved_url = url_format.format(pn = pub_number, hc = required_batch_size, rs = current_index + 1)

	data = requests.get( resolved_url )
	bs = BeautifulSoup( data.text, 'xml' )
	if bs.root is None:
		return []
	total = bs.root.totalfound
	articles = []
	for doc in bs.find_all("document"):
		articles.append(parse_document(doc))

	if articles == []:
		return []
	else:
		return articles + fetch_all_articles( pub_number, batch_size, current_index + required_batch_size, limit )
