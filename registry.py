import citeseerx
import ieee
import springer
import wiley
from functools import partial

_REGISTRY = {

    "CardioVascular and Interventional Radiology" : partial( springer.parse_journal, "http://link.springer.com/journal/volumesAndIssues/270"),
    "European Radiology" : partial( springer.parse_journal, "http://link.springer.com/journal/volumesAndIssues/330"),
    # "Journal of Digital Imaging" : partial( springer.parse_journal, "http://link.springer.com/journal/volumesAndIssues/10278" ),
    # "Annals of Biomedical Engineering" : partial( springer.parse_journal, "http://link.springer.com/journal/volumesAndIssues/10439" ),
    "International Journal of CardioVascular Imaging" : partial( springer.parse_journal, "http://link.springer.com/journal/volumesAndIssues/10554"),
    # "Journal of Mathematical Imaging and Vision" : partial( springer.parse_journal, "http://link.springer.com/journal/volumesAndIssues/10851" ),
    # "International Journal of Computer Vision" : partial( springer.parse_journal, "http://link.springer.com/journal/volumesAndIssues/11263"),
    # "Medical and Biological Engineering and Computing" : partial( springer.parse_journal, "http://link.springer.com/journal/volumesAndIssues/11517" ),
    "International Journal of Computer Assisted Radiology and Surgery" : partial( springer.parse_journal, "http://link.springer.com/journal/volumesAndIssues/11548" ),

    # "Insights into Imaging" : partial( springer.parse_journal, "http://link.springer.com/journal/volumesAndIssues/13244" ),
    # "Japanese Journal of Radiology" : partial( springer.parse_journal, "http://link.springer.com/journal/volumesAndIssues/11604" ),
    # "Current Radiology Reports" : partial( springer.parse_journal, "http://link.springer.com/journal/volumesAndIssues/40134" ),
    # "Oral Radiology" : partial( springer.parse_journal, "http://link.springer.com/journal/volumesAndIssues/11282"),
    # "Pediatric Radiology" : partial( springer.parse_journal, "http://link.springer.com/journal/volumesAndIssues/247"),
    # "Emergency Radiology" : partial( springer.parse_journal, "http://link.springer.com/journal/volumesAndIssues/10140"),
    # "Abdominal Radiology" : partial( springer.parse_journal, "http://link.springer.com/journal/volumesAndIssues/261"   ),

    # "IEEE Transactions on Biomedical Engineering" : partial( ieee.fetch_all_articles, 10 ),
    # "IEEE Transactions on Pattern Analysis and Machine Intelligence" : partial( ieee.fetch_all_articles, 34 ),
    "IEEE Transactions on Medical Imaging" : partial( ieee.fetch_all_articles, 42 ),
    # "IEEE Transactions on Image Processing" : partial( ieee.fetch_all_articles, 83 ),
    # "IEEE Transactions on Neural Networks and Learning Systems" : partial( ieee.fetch_all_articles, 5962385),
    # "IEEE Journal of Biomedical and Health Informatics" : partial( ieee.fetch_all_articles, 6221020 ),
    "PacificVis" : partial( ieee.fetch_all_articles, 7460639 ),

    "Magnetic Resonance in Medicine" : partial( wiley.parse_journal, "http://onlinelibrary.wiley.com/journal/10.1002/(ISSN)1522-2594/issues" ),
    "Journal of Magnetic Resonance Imaging" : partial( wiley.parse_journal, "http://onlinelibrary.wiley.com/journal/10.1002/(ISSN)1522-2586/issues" ),
    "Computer Graphics Forum" : partial( wiley.parse_journal, "http://onlinelibrary.wiley.com/journal/10.1111/(ISSN)1467-8659/issues")
}
