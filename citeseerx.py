from oaipmh.client import Client
from oaipmh.metadata import MetadataRegistry, oai_dc_reader
import requests
import grequests
from bs4 import BeautifulSoup
import urlparse

"""
	This first section uses the publicly available Open Archives Initiative https://www.openarchives.org/
	to get details about any paper - not a bad api for that purpose. Look how compact the code is.

"""
ROOT_URL = "http://citeseerx.ist.psu.edu"

OAI_ROOT_URL = ROOT_URL + "/oai2"

def make_client():
    registry = MetadataRegistry()
    registry.registerReader("oai_dc", oai_dc_reader)
    return Client(OAI_ROOT_URL, registry)

OAI_CLIENT = make_client()

IDENTIFIER_FORMAT = "oai:CiteSeerX.psu:{}"

def query_paper(doi):
    # don't know what the third thing in the tuple is!
    header, metadata, about = OAI_CLIENT.getRecord(metadataPrefix='oai_dc', identifier = IDENTIFIER_FORMAT.format(doi))
    return metadata.getMap()

"""
	This code goes back to the tried and tested approach of scraping the site - primarily because most of the data
	they have isn't mentioned in the OAI.
	Keyphrases, citations (forward/back/some peer definitions), download links, 

"""

PAPER_URL_FORMAT = ROOT_URL + "/viewdoc/summary"

def add_query(url, q):
    return url if q is None else url + "?" + requests.compat.urlencode(q)

CITATIONS_URL_FORMAT = ROOT_URL + "/viewdoc/citations"

def get_doi_from_resp(resp):
    last_url = get_url_from_redirect_response(resp)
    parse_result = requests.compat.urlparse( last_url )
    q = parse_result.query
    q_dict = urlparse.parse_qs( q )
    return q_dict['doi'][0]

def parse_citations(doi, batch_size = 5):
    citations_url = add_query(CITATIONS_URL_FORMAT, {"doi" : doi} )
    response = requests.get( citations_url )
    soup = BeautifulSoup(response.text)
    citations_div = soup.find(id="citations")
    # citeseerx has two families of links when a document cites another
    # /viewdoc/summary?cid={} for when citeseerx has a page for the paper
    # /showciting?cid={} for when citeseerx just has a record of citations
    # let's just deal in the first batch because then we can get 
    cite_links = ( x['href'] for x in citations_div.find_all("a") if "viewdoc" in x['href'] )
    cite_real_links = [ requests.compat.urljoin( citations_url, ln) for ln in cite_links ]
    # need to resolve these links to get dois, rather than citeseers' internal ids
    # ... but I'd rather find some way to set a policy to get requests to just follow redirects then
    # give up ... but I think citeseerX detects if we permit redirection - and if not just serves from
    # the original link
    cited_dois = []
    hooks = {'response': lambda resp, *args, **kwargs: cited_dois.append(get_doi_from_resp(resp))}
    # hooks = {'response': lambda resp, *args, **kwargs: cited_dois.append(resp)}
    reqs = [ grequests.head(c, hooks = hooks) for c in cite_real_links ]
    grequests.map( reqs, size = batch_size )
    return cited_dois

def get_url_from_redirect_response(resp):
    """
        This code is absurdly complicated - because requests doesn't
        seem to expose a function to do this, but this is exactly the code
        that's used in requests.Session .. except for perhaps function version
        - so should replicate the logic fairly well :)
    """
    try:
        resp.content  # Consume socket so it can be released
    except (ChunkedEncodingError, ContentDecodingError, RuntimeError):
        resp.raw.read(decode_content=False)

    # Release the connection back into the pool.
    resp.close()

    url = resp.headers['location']

    # Handle redirection without scheme (see: RFC 1808 Section 4)
    if url.startswith('//'):
        parsed_rurl = urlparse(resp.url)
        url = '%s:%s' % (parsed_rurl.scheme, url)

    # The scheme should be lower case...
    parsed = urlparse.urlparse(url)
    url = parsed.geturl()
    return url


def parse_paper(doi = None, cid = None):
    assert doi is not None or cid is not None
    paper_url = add_query( PAPER_URL_FORMAT, {"cid" : cid } if doi is None else {"doi" : doi} )
    response = requests.get( paper_url )
    # note if we were a cid, (came from a citation) then citeseer will redirect to a doi url
    # we store this as we will use it when we want to change page and look at other aspects
    # (commented as ... it appears without redirects history may be empty )
    # final_url = response.history[-1].url

    paper_soup = BeautifulSoup(response.text)
    kw_div = paper_soup.find(id="keywords")
    keywords = [ a.text for a in kw_div.find_all("a") ]
    return paper_soup